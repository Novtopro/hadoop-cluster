# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV['VAGRANT_DEFAULT_PROVIDER'] = 'virtualbox'.freeze
VAGRANTFILE_API_VERSION         = '2'.freeze
BOX                             = 'bento/ubuntu-16.10'.freeze
BRIDGE                          = 'en0: Wi-Fi (AirPort)'.freeze

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.ssh.insert_key = false

  config.vm.provider 'virtualbox' do |virtualbox|
    virtualbox.gui          = false
    virtualbox.cpus         = 4
    virtualbox.memory       = 512
    virtualbox.linked_clone = true
    virtualbox.customize ['modifyvm', :id, '--ioapic', 'on']
  end

  config.vm.define 'nn-1' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'nn-1.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2210
    node.vm.network :private_network, ip: '192.168.2.10'
    node.vm.network :public_network, ip: '192.168.0.10', bridge: BRIDGE
  end

  config.vm.define 'nn-2' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'nn-2.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2211
    node.vm.network :private_network, ip: '192.168.2.11'
    node.vm.network :public_network, ip: '192.168.0.11', bridge: BRIDGE
  end

  config.vm.define 'dn-1' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'dn-1.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2214
    node.vm.network :private_network, ip: '192.168.2.12'
    node.vm.network :public_network, ip: '192.168.0.12', bridge: BRIDGE
  end

  config.vm.define 'dn-2' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'dn-2.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2215
    node.vm.network :private_network, ip: '192.168.2.13'
    node.vm.network :public_network, ip: '192.168.0.13', bridge: BRIDGE
  end

  config.vm.define 'dn-3' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'dn-3.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2216
    node.vm.network :private_network, ip: '192.168.2.14'
    node.vm.network :public_network, ip: '192.168.0.14', bridge: BRIDGE
  end

  config.vm.define 'dn-4' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'dn-4.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2217
    node.vm.network :private_network, ip: '192.168.2.15'
    node.vm.network :public_network, ip: '192.168.0.15', bridge: BRIDGE
  end

  config.vm.define 'dn-5' do |node|
    node.vm.box      = BOX
    node.vm.hostname = 'dn-5.hadoop-cluster.com'
    node.vm.network :forwarded_port, guest: 22, host: 2218
    node.vm.network :private_network, ip: '192.168.2.16'
    node.vm.network :public_network, ip: '192.168.0.16', bridge: BRIDGE
  end

  # the most common configuration options are documented and commented below.
  # for a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # every vagrant development environment requires a box. you can search for
  # boxes at https://atlas.hashicorp.com/search.
  # config.vm.box = "base"

  # disable automatic box update checking. if you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. this is not recommended.
  config.vm.box_check_update = false

  # create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. in the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # note: this will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # create a private network, which allows host-only access to the machine
  # using a specific ip.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # create a public network, which generally matched to bridged network.
  # bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # share an additional folder to the guest vm. the first argument is
  # the path on the host to the actual folder. the second argument is
  # the path on the guest to mount the folder. and the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # provider-specific configuration so you can fine-tune various
  # backing providers for vagrant. these expose provider-specific options.
  # example for virtualbox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
