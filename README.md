# Hadoop Cluster

Building a Hadoop cluster for learning.

## Setup

1. Install [Vagrant](https://www.vagrantup.com/) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2. Run `vagrant plugin install vagrant-hostsupdater`
3. [Enable passwordless sudo](https://github.com/cogitatio/vagrant-hostsupdater#passwordless-sudo)
5. Update network bridge in Vagrantfile if needed
6. Run `vagrant up`

## See also

1. https://github.com/cogitatio/vagrant-hostsupdater
